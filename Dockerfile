FROM amazonlinux:2017.03
# Amazon Linux with Miniconda 3 for Dee

RUN yum install -y wget bzip2

# Install miniconda3 and add to path
RUN echo 'export PATH=/opt/conda/bin:$PATH' > /etc/profile.d/conda.sh && \
    wget --quiet https://repo.continuum.io/miniconda/Miniconda3-4.3.21-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/conda && \
    rm ~/miniconda.sh && \
    yum clean all

# Add conda to path
ENV PATH /opt/conda/bin:$PATH

# Install dee requirements into the root environment
ADD environment.yml /environment.yml
RUN conda env update -n root -f /environment.yml && \
    conda clean -y -t

# Set the working directory to /app
WORKDIR /app
COPY . /app