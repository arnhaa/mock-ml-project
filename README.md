# Mock ML Project

Mock Machine Learning Project used for testing a code deployment pipeline.

## Setup instructions

### Install awscli
The deployment machine needs to have awscli installed, in order for pushing of built docker images to work.
If pip is installed on the machine you can run:
 
    pip install awscli --upgrade --user
    
Other options for installing can be found [here](http://docs.aws.amazon.com/cli/latest/userguide/installing.html).

### Configure awscli
When awscli is installed, it must be configured. To do this run:

    aws configure

### Set the identifier of the docker repo
You also need to set the identifier of the aws docker repo.
To do this, set the environment variable AWS_ECR_REPO to the identifier.

E.g. on windows the environment variable can be set in the following way.

    setx AWS_ECR_REPO=123123123123.dkr.ecr.eu-west-1.amazonaws.com
    

E.g. on linux, it can be set by adding this line to the end of ~/.bashrc:

    export AWS_ECR_REPO=123123123123.dkr.ecr.eu-west-1.amazonaws.com

Remember to replace the value with the actual identifier of the ECR repo.

### Install make

On linux

    sudo apt-get install build-essential
    
On Windows, follow the instructions given [here](http://gnuwin32.sourceforge.net/packages/make.htm)


## Usage instructions

### Building the docker image

    make build

### Running the tests in the built image

    make test
    
### Pushing the image to ECR

    make push-aws
