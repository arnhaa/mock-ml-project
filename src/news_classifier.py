from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import LinearSVC
from sklearn.pipeline import make_pipeline
import numpy as np
import os
import joblib

dir_path = os.path.dirname(os.path.realpath(__file__))
MODEL_PICKLE_FILE = os.path.join(dir_path, "news_predictor_model.pkl")

TYPE_LIST_LIKE = (np.ndarray, list, set, tuple)


def _create_text_classifier():
    """
    Create a text classifier pipeline.

    Returns:
        sklearn.pipeline.Pipeline: The classifier.
    """

    vec = TfidfVectorizer(min_df=2, max_df=0.5, max_features=10000, ngram_range=(1, 2), sublinear_tf=True,
                          stop_words="english")
    clf = LinearSVC(class_weight="balanced")
    return make_pipeline(vec, clf)


def _create_and_fit_news_classifier():
    """
    Create a text classifier, fetch news data, and fit the classifier on this data.
    Finally, store the fitted model.

    Returns:
        None
    """

    print("Creating text classifier")
    model = _create_text_classifier()

    print("Fetching news data")
    news = fetch_20newsgroups()

    print("Fitting classifier")
    model.fit(news.data, news.target)

    print("Storing fitted classifier")
    joblib.dump(model, MODEL_PICKLE_FILE)


def _get_target_names():
    """
    Get the news category names.

    Returns:
        np.ndarray: The target names
    """

    news = fetch_20newsgroups()
    return np.asarray(news.target_names)


class NewsClassifier():
    def __init__(self):
        """
        A classifier used to classify the topic of news articles.
        """

        if not os.path.exists(MODEL_PICKLE_FILE):
            _create_and_fit_news_classifier()

        print("Loading fitted classifier")
        self.model = joblib.load(MODEL_PICKLE_FILE)
        self.target_names = _get_target_names()

    def classify(self, texts):
        """
        Classify the topic for the given text(s)

        Args:
            texts: A single text or a collection of texts.

        Returns:
            str: The classified topic name(s)
        """

        if isinstance(texts, TYPE_LIST_LIKE):
            return self.target_names[self.model.predict(texts)]
        elif isinstance(texts, str):
            return self.target_names[self.model.predict([texts])[0]]
        raise TypeError("Invalid type for texts <{}>".format(type(texts)))


if __name__ == '__main__':
    news_classifier = NewsClassifier()
    print(news_classifier.classify("I have an amazing windows 10 pc"))